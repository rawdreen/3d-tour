﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIController : MonoBehaviour
{

    public GameObject helpPanel;
    public GameObject contactsPanel;
    public Text hintText;
    public Text timeText;

    public static bool checkHelp = false;
    public static bool checkContact = false;
    public static bool checkHint = false;



    [Header("Words for hint & duration")]
    public static int maxWords = 12;
    public string[] hint = new string[maxWords];
    static int currentHint;



    // Use this for initialization
    void Start()
    {
        helpPanel.gameObject.SetActive(false);
        contactsPanel.gameObject.SetActive(false);
        hintText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        helpPanel.gameObject.SetActive(checkHelp);
        contactsPanel.gameObject.SetActive(checkContact);

        timeText.text = DateTime.Now.Hour.ToString() + " : " + DateTime.Now.Minute.ToString() + " : " + DateTime.Now.Second.ToString();

        Hint();

    }


    public static void GetCurrentHint(int i)
    {
        currentHint = i;
    }


    public void Hint()
    {


        if (checkHint == true)
        {
            hintText.text = "HINT\nSay: " + hint[currentHint];
            hintText.enabled = true;
        }
        else
        {

            hintText.enabled = false;
        }

    }
}
