﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpsLibrary : MonoBehaviour {

    public GameObject library;
    [Header("Main Menu Commands")]
    public  string [] main_menu = new string[4];
    public string[] main_menu_btn = new string[4];

    [Header("Contacts Menu Commands")]
    public  string[] contatcs_menu = new string[1];
    public string[] contatcs_menu_btn = new string[1];

    [Header("Map Menu Commands")]
    public  string[] map_menu = new string[1];
    public string[] map_menu_btn = new string[1];


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string Get_Main_Menu_Help(int i)
    {
        HelpPanelControl.MaxRange = main_menu.Length;
        return main_menu[i];
    }

    public string Get_Contacts_Menu_Help(int i)
    {
        HelpPanelControl.MaxRange = contatcs_menu.Length;
        return contatcs_menu[i];
    }

    public string Get_Map_Menu_Help(int i)
    {
        HelpPanelControl.MaxRange = map_menu.Length;
        return map_menu[i];
    }

    public string Get_Main_Menu_btn(int i)
    {
        return main_menu_btn[i];
    }

    public string Get_Contacts_Menu_btn(int i)
    {
        return contatcs_menu_btn[i];
    }

    public string Get_Map_Menu_btn(int i)
    {
        return map_menu_btn[i];
    }
}
