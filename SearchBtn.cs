﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchBtn : MonoBehaviour {

    public GameObject searchPanel;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        if(GameObject.FindGameObjectWithTag("SearchPanel")==false)
        {
            Transform tmpParent = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Transform>();
            GameObject tmpObj = Instantiate(searchPanel, tmpParent);
        }
        else
        {
            GameObject.FindGameObjectWithTag("SearchPanel").GetComponent<SearchPanel>().m_Recognizer.Dispose();
            Destroy(GameObject.FindGameObjectWithTag("SearchPanel"));
        }
    }
}

