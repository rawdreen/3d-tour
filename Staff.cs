﻿using System;
using System.Collections.Generic;
using System.Text;


class Staff
{
    public string name { get; set; }
    public string sureName { get; set; }

    public string department { get; set; }

    public string mail { get; set; }

    public string room { get; set; }
    public string officeHours { get; set; }
    public string post { get; set; }

    public Staff(string name, string sureName, string department, string mail, string room, string officeHours, string post)
    {
        this.name = name;
        this.sureName = sureName;
        this.department = department;
        this.mail = mail;
        this.room = room;
        this.officeHours = officeHours;
        this.post = post;
    }

    public Staff(string name)
    {
        this.name = name;
    }

    public string ReturnStaff()
    {
        return name;
    }
}
