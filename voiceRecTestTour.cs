﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class voiceRecTestTour : MonoBehaviour {

    KeywordRecognizer keyWord;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    [Header("Active Cameras")]
    public static int camSize = 2;
    public Camera[] cameras = new Camera[camSize];
    public TimelineAsset timeline;
    public PlayableDirector pl;

    [Header("Audios & Text")]
    public static int size = 2;
    public AudioSource[] voices = new AudioSource[size];

    [Header("Animation")]
    public Animator rig;



    public Color colorStart = Color.red;
    public Color colorEnd = Color.green;
    public float duration = 1.0F;
    public Renderer[] rend = new Renderer[2];


	// Use this for initialization
    void Start()
    {

        keywords.Add("information about University", Hello);
        keywords.Add("back to start", Change);

        keyWord = new KeywordRecognizer(keywords.Keys.ToArray());
        keyWord.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keyWord.Start();
    }
    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }


    void Hello()
    {
        voices[2].Play();
        rig.Play("accepted");
        cameras[0].rect = UnityEngine.Rect.MinMaxRect(0.8f, 0, 1, 0.37f);
        pl.Play();
    }
	
	// Update is called once per frame
	void Update () 
    {
        


        Key();
	}

    void Key()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            cameras[0].rect = UnityEngine.Rect.MinMaxRect(0, 0, 1, 1);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            Hello();
            
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            rig.Play("accepted");
            Invoke("Change", 2f);
            cameras[0].rect = UnityEngine.Rect.MinMaxRect(0, 0, 1, 1);
           

        }

    }
    void Change()
    {
        SceneManager.LoadScene(0);
    }
}
