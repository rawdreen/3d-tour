﻿using UnityEngine;
using UnityEngine.UI;

public class HelpCommandPanel : MonoBehaviour {

    public Text commandName;
    public Text btnName;
    public RawImage btnImage;


	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetComponentsValues(string commandName, string btnName)
    {
        this.commandName.text = commandName;
        this.btnName.text = btnName;
    }
}
