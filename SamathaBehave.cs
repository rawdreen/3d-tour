﻿using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine;
using System;

/// <summary>
/// Welcome to the Control Board of Samantha 
/// Here all Samantha`s logic is collected
/// </summary>

public class SamathaBehave : MonoBehaviour
{

    [Header("Sub Objects")]
    public GameObject[] element = new GameObject[3];
    public AudioSource[] track;
    public Transform location;
    public Transform parent;
    GameObject tmpObj;


    private bool sub_obj_pressed = false;

    private bool is_awake = true;

    public static bool first_start = true;
    public static bool contactListAccessed = false;

    [Header("Elements to be called")]
    public Clock clock;
    public GameObject weather;
    public GameObject hint;
    public GameObject notify;

    private bool element_called = false;
    private string last_called_element;

    [Header("Timer for listening")]
    public float initialTime = 40f;
    public float listenTime = 20f;
    public float hintTime = 5f;
    private float waitRequest;

    KeywordRecognizer keyword;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();




    // Use this for initialization
    void Start()
    {

        //setting all UI components and Awake-Timer to their initial values
        hint.SetActive(false);
        notify.SetActive(false);

        //Adding keywords for my Dictionary
        keywords.Add("what time is it", TimeRequest);
        keywords.Add("what is the weather today", WeatherRequest);
        keywords.Add("Give me a hint", HintRequest);
        keywords.Add("Wake up", Awake);
        keywords.Add("Are you here", Awake);
        keywords.Add("How are you?", Fine);
        keywords.Add("What is your name?", Name);
        keywords.Add("How old are you?", Age);
        keywords.Add("Who created you?", Genesis);
        keywords.Add("What is your hobby?", Hobby);
        keywords.Add("What is your dream?", Dream);
        keywords.Add("Good bye!", Bye);
        keywords.Add("Show me a map", ShowMeMap);
        keywords.Add("Contact list", ContactList);

        keyword = new KeywordRecognizer(keywords.Keys.ToArray());
        keyword.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keyword.Start();

        if(first_start)
        {
            waitRequest = initialTime;
            track[1].Play();
        }
        else
        {
            waitRequest = listenTime;
            track[11].Play();
        }

    }

    void Fine()
    {
        Accepted();
        track[4].Play();
    }

    void Name()
    {
        Accepted();
        track[5].Play();
    }

    void Age()
    {
        Accepted();
        track[6].Play();
    }

    void Genesis()
    {
        Accepted();
        track[7].Play();
    }

    void Hobby()
    {
        Accepted();
        track[8].Play();
    }

    void Dream()
    {
        Accepted();
        track[9].Play();
    }

    void Bye()
    {
        Accepted();
        track[10].Play();
    }


    void ShowMeMap()
    {
        SceneManager.LoadScene(3);
    }

    void ContactList()
    {
        SceneManager.LoadScene(2);
    }


    void Accepted()
    {
        DestroySubObj();
        location.GetComponent<Animator>().Play("accepted");
        UIReset();
    }



    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if(keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }



    // Update is called once per frame
    void Update()
    {
        WaitTime();
        KeyBoard();
    }


    //Indicator`s active time
    void WaitTime()
    {
        waitRequest -= Time.deltaTime;
        if (waitRequest <= 0)
        {
            SleepMode();
        }else
            if(waitRequest < listenTime-hintTime )
        {
            HintRequest();
        }
    }

     public void UIReset()
    {
        waitRequest = listenTime;
        hint.SetActive(false);
    }

    //Actions performed when Indicator is pressed
    void OnMouseDown()
    {
        UIReset();
        track[0].Play();
        if(first_start)
        {
            track[2].Play();
            first_start = false;
        }

        float radius = 2f;
        if (sub_obj_pressed==false)
        {
            
            location.GetComponent<Animator>().Play("pressed");
            this.GetComponent<MeshRenderer>().enabled = true;
            Debug.Log("Pressed");
            for (int i = 0; i < element.Length; i++)
            {
                float angle = i * Mathf.PI * 2f / element.Length;
                Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius);
                tmpObj = Instantiate(element[i], newPos, Quaternion.identity);
                tmpObj.transform.SetParent(parent);
                tmpObj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                
            }
            sub_obj_pressed = true;
        }
        else
        {
            
            location.GetComponent<Animator>().Play("released");
            DestroySubObj();
        }
    }


    //Destroy sub-menus when they are not used
    public void DestroySubObj()
    {
        this.GetComponent<MeshRenderer>().enabled = false;
        if (sub_obj_pressed == true)
        {
            for (int i = 0; i < GameObject.FindGameObjectsWithTag("box").Length; i++)
            {
                GameObject[] obj = GameObject.FindGameObjectsWithTag("box");
                Destroy(obj[i]);
            }
        }
        sub_obj_pressed = false;
    }


    // used when I request to do some task
    void ElementCalled(string element_type)
    {

        if (sub_obj_pressed==false)
        {
            track[0].Play();
            DestroySubObj();
            switch (element_type)
            {
                case "clock":
                    {
                        last_called_element = element_type;
                        //element_called = true;

                        location.GetComponent<Animator>().Play("clock");
                        Clock tmpClock = Instantiate(clock, parent);
                        tmpClock.transform.SetParent(parent);
                        Destroy(GameObject.FindGameObjectWithTag(element_type),5f);
                        break;
                    }
                case "hint":
                    {
                        //element_called = true;
                        last_called_element = element_type;
                        hint.SetActive(true);
                        //GameObject tmpHint = Instantiate(hint, parent);
                        //last_called_element = element_type;
                        //Destroy(GameObject.FindGameObjectWithTag(element_type), 5f);
                        break;
                    }
                case "weather":
                    {
                        last_called_element = element_type;
                        GameObject tmpWeather = Instantiate(weather, parent);
                        last_called_element = element_type;
                        Destroy(GameObject.FindGameObjectWithTag(element_type),1f);
                        break;
                    }

                   
            }
            
        }
    }

    private void WeatherRequest()
    {
        Accepted();
        ElementCalled("weather");
    }

    private void TimeRequest()
    {
        Accepted();
        ElementCalled("clock");
    }

    private void HintRequest()
    {
        hint.SetActive(true);
        ElementCalled("hint");
    }


    //Wake Indicator up
    private void Awake()
    {
        if(is_awake==false)
        {
            is_awake = true;
            track[11].Play();
            location.GetComponent<Animator>().Play("start");
            UIReset();
        }
        notify.SetActive(false);
    }



    //keyboard control for convinience
    void KeyBoard()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ElementCalled("clock");
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            ElementCalled("weather");
        }
        if(Input.GetKeyDown(KeyCode.Space))
        { 
            Awake(); 
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Fine();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            Name();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Age();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Genesis();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Hobby();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Dream();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Fine();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            ShowMeMap();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ContactList();
        } 
        if (Input.GetKeyDown(KeyCode.B))
        {
            Bye();
        }
    }


    //Indicator goes to sleep
    void SleepMode()
    {
        is_awake = false;
        location.GetComponent<Animator>().Play("escape");
        hint.SetActive(false);                              //each time in each function related to interactions I have to use it
        
        notify.SetActive(true);


        DestroySubObj();
    }
}
