﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;
using System.Linq;
using System.Data;
using Mono.Data.Sqlite;


public class SearchPanel : MonoBehaviour
{

    public RawImage indicator;
    public InputField inputField;
    public Button searchBtn;
    public Text notification;
    AudioSource recognized;

    public GameObject thisObj;
    public GameObject departmentController;
    public GameObject staffPrefab;

    private string staffMember;



    private List<Staff> staff = new List<Staff>();
    private string connectionString;


    static int max_size;
    public string[] m_Keywords = {};

    public KeywordRecognizer m_Recognizer;
    void Start()
    {
        recognized = GameObject.FindGameObjectWithTag("recognized").GetComponent<AudioSource>();

        connectionString = "URI= file:" + Application.dataPath + "/Database/Staff.sqlite";

        max_size = GetSizeOfDB();
        GetDataName();

        Debug.Log("DB OPENED");

        //SetKeyWords();
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        m_Recognizer.Start();
        recognized.Play();
        indicator.GetComponent<Animation>().Rewind();

    }



    public int GetSizeOfDB()
    {
        connectionString = "URI= file:" + Application.dataPath + "/Database/Staff.sqlite";

        int max_size = 0;
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT COUNT(*) as total FROM staff";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        max_size = reader.GetInt32(0);
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }

        }
        return max_size;
    }


    public void GetDataName()
    {
        staff.Clear();
        DestroyData();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT name FROM staff";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    m_Keywords = new string[max_size];
                    int i = 0;
                    while (reader.Read())
                    {
                        staff.Add(new Staff(reader.GetString(0)));
                        m_Keywords[i] = reader.GetString(0);
                        Debug.Log(m_Keywords[i]);
                        i++;
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }


        }
    }

     public void GetNameData(string recognizedName)
    {
        Debug.Log("Entered ND");
        staff.Clear();
        Debug.Log("Cleared staff");
        DestroyData();
        Debug.Log("destroyed PrevDB");
       
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT * FROM staff WHERE name ='" + recognizedName + "'";
                Debug.Log("Entered into GetNameData");
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    Debug.Log("db Command is Opened");
                    while (reader.Read())
                    {
                        Debug.Log("Took the variables");
                        staff.Add(new Staff(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6)));
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }


        }

        for (int i = 0; i < staff.Count; i++)
        {
            Debug.Log(m_Keywords[i]);
        }

        //m_Keywords = new string[0];
        //for (int i = 0; i < max_size; i++)
        //{
        //    m_Keywords[i] = staff[i].ReturnStaff();
        //}

        for (int i = 0; i < staff.Count; i++)
        {
            Debug.Log(m_Keywords);
        }
    }


    void SetData(string recognizedName)
    {
        Debug.Log("Entered SD");
            GetNameData(recognizedName);

            for (int i = 0; i < staff.Count; i++)
            {
                Debug.Log("Instatiating GetNameData");
                GameObject tmpObj = Instantiate(staffPrefab);
                Staff tmpStaff = staff[i];

                tmpObj.GetComponent<StaffData>().SetStaffValues(tmpStaff.name, tmpStaff.sureName, tmpStaff.department, tmpStaff.mail, tmpStaff.room, tmpStaff.officeHours, tmpStaff.post);
                tmpObj.transform.SetParent(GameObject.FindGameObjectWithTag("ContactsPanel").GetComponent<Transform>());
                tmpObj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            }
            if (staff.Count <= 0)
            {
                Debug.Log("No data found");
            }
            DestroyThisObj();
    }

    public void DestroyData()
    {
        Debug.Log("Destroying Previous data");
        for (int i = 0; i < GameObject.FindGameObjectsWithTag("contacts").Length; i++)
        {
            Debug.Log("Destroyed");
            GameObject[] obj = GameObject.FindGameObjectsWithTag("contacts");
            Destroy(obj[i]);
        }
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        notification.text = args.text;
        inputField.text = args.text;

        
        StartCoroutine(Wait(args.text));
        
    }


    IEnumerator Wait(string args)
    {
        Debug.Log(args);
        recognized.Play();
        indicator.GetComponent<Animation>().Play();
       yield return  new WaitForSeconds(1f);

       SetData(args);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetInFieldData()
    {
        staffMember = inputField.text;
        GameObject.FindGameObjectWithTag("DepartmentController").GetComponent<DataBase>().GetDataThroughName(staffMember);
        DestroyThisObj();
    }

    public void DestroyThisObj()
    {
        m_Recognizer.Dispose();
        Destroy(thisObj);
    }
}