﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;
using UnityEngine.SceneManagement;


public class GuideController : MonoBehaviour
{

    bool checkHelp = false;
    bool checkContact = false;
    static bool checkStarted = false;
    public static bool samPressed = false;
    bool checkHint = false;

    KeywordRecognizer keyWord;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();


    //Header - used for simplification of Unity interface

    [Header("Audios")]
    public static int CHsize = 9;
    public static int CMsize = 5;
    public static int FXsize = 5;


    public AudioSource[] chatVoice = new AudioSource[CHsize];
    public AudioSource[] cmVoice = new AudioSource[CMsize];
    public AudioSource[] fxVoice = new AudioSource[FXsize];

    [Header("Animation")]
    public Animator rig;

    [Header("Timer")]
    public float countdown = 60f;
    public float nextDuration = 60f;

    // Used for initialization #InvokesOnes
    void Start()
    {
        keywords.Add("Hello", Hello);
        keywords.Add("how are you", Fine);
        keywords.Add("What is your name?", Samantha);
        keywords.Add("tell me a joke", Joke);
        keywords.Add("What do you like?", Taste);
        keywords.Add("What is your dream", Dream);
        keywords.Add("Are you here?", Ready);
        keywords.Add("It`s nice to meet you", NiceToMeet);
        keywords.Add("Good bye", Bye);


        keywords.Add("commands", Help);
        keywords.Add("Contact list", Contacts);
        keywords.Add("What is the weather today?", Weather);
        keywords.Add("show me the map", Change);

        if(checkStarted==false)
        {
            Hello();
            Invoke("Instructions", 10f);
            checkStarted = true;
        }else{
            fxVoice[0].Play();
            chatVoice[10].Play();
        }
        

        keyWord = new KeywordRecognizer(keywords.Keys.ToArray());
        keyWord.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keyWord.Start();


        
    }


    //Recognizer
    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }



    // List of functions of a Guide

    //Chat related
    void Hello()
    {
        chatVoice[0].Play();
        rig.Play("start");
    }

    void Fine()
    {
        fxVoice[0].Play();
        chatVoice[1].Play();
        rig.Play("accepted");
        SetFalse();
        
    }

    void Samantha()
    {
        chatVoice[2].Play();
        rig.Play("accepted");
        SetFalse();
    }

    void Joke()
    {
        chatVoice[3].Play();
        rig.Play("accepted");
        SetFalse();
    }
    void Taste()
    {
        chatVoice[4].Play();
        rig.Play("accepted");
        SetFalse();
    }

    void Dream()
    {
        chatVoice[5].Play();
        rig.Play("accepted");
        SetFalse();
    }
    void Ready()
    {
        fxVoice[0].Play();
        chatVoice[6].Play();
        rig.Play("accepted");
        samPressed = false;
        SetFalse();
    }

    void NiceToMeet()
    {
        chatVoice[7].Play();
        rig.Play("accepted");
        SetFalse();
    }

    void Bye()
    {
        chatVoice[8].Play();
        float countdown = 10f;
        countdown -= Time.deltaTime;
        rig.Play("escape");
        SetFalse();
    }


//CommandRelated
    void Instructions()
    {
        cmVoice[0].Play();
        //rig.Play("accepted");
        
    }

    public void Help()
    {
        if(checkHelp==false)
        {
            cmVoice[1].Play();
            //Invoke("StopVoice(1)", 5f);
        }
            
        
        CommandAccepted();
        UIController.checkContact = false;
        checkContact = false;
    }

    void StopVoice(int i)
    {
        if(cmVoice[i].enabled == true)
        {
            cmVoice[i].gameObject.SetActive(false);
        }
        else
        {
            cmVoice[i].gameObject.SetActive(true);
        }
    }

    public void Contacts()
    {
        //System.Diagnostics.Process.Start("http://www.google.com");
        if (checkContact == false)
        {
            cmVoice[2].Play();
            //Invoke("StopVoice(2)", 5f);
        }
        ContactList();
        UIController.checkHelp = false;
        checkHelp = false; 

    }

    public void Weather()
    {
        cmVoice[3].Play();
        rig.Play("accepted");
        SetFalse();
    }

    public void Change()
    {
        SceneManager.LoadScene(1);
    }



    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown<=nextDuration/2f && countdown>=0f && checkHint==false)
        {
            int currentHint = Random.Range(0, UIController.maxWords - 1);
            UIController.GetCurrentHint(currentHint);
            UIController.checkHint = true;
            checkHint=true;

        }else if(countdown<=0f && checkHint==true)
        {
            UIController.checkHint = false;
            checkHint = false;
            rig.Play("escape");
        }
        Key();

        if (samPressed == true)
            Ready();

    }


    // Key() - Collection of buttons which gives ability to interact with guide using keyboard
    void Key()
    {

        if (Input.GetKeyDown(KeyCode.F))
        {
            chatVoice[9].Play();
            Invoke("Change", 2f);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            countdown = nextDuration;
            Hello();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            Ready();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Fine();
        }

    }


    //UIControl - sends value to ui components in case command accepted

    void CommandAccepted()
    {
        if (checkHelp != false)
        {
            UIController.checkHelp = false;
            checkHelp = false; 
        }
        else
        {
            UIController.checkHelp = true;
            checkHelp = true;
        }
    }

    void ContactList()
    {
        if(checkContact!=false)
        {
            UIController.checkContact = false;
            checkContact = false;
        }
        else
        {
            UIController.checkContact = true;
            checkContact = true;
        }
    }

    void SetFalse()
    {
        countdown = nextDuration;
        
        checkHelp = false;
        checkContact = false;
        UIController.checkHelp = false;
        UIController.checkContact = false;
    }
}

