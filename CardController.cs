﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour {

    public GameObject thisObj;

    public Text name;
    public Text sureName;
    public Text mail;
    public Text post;
    public Text office;
    public Text officeHours;

    public RawImage avatar;

    public void Accept(string name, string sureName, string mail, string post, string office, string officeHours)
    {
        this.name.text += " " + name;
        this.sureName.text += " " + sureName;
        this.mail.text += " " + mail;
        this.post.text += " " + post;
        this.office.text += " " + office;
        this.officeHours.text += " " + officeHours;
    }

    public void WindowClose()
    {
        Debug.Log("Destroy");
        Destroy(thisObj);
    }


	void Start () {
	}
	
}
