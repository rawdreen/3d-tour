﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepartmentContainer : MonoBehaviour
{

    public string [] departmentType= new string[3];

    public string GetDepartmentType(int i)
    {
        return departmentType[i];
    }
}
