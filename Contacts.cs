﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Contacts : MonoBehaviour {

    public int loadingSceneIndex = 2;
    AudioSource accepted;
    

	// Use this for initialization
	void Start () {
        accepted = GameObject.FindGameObjectWithTag("popup").GetComponent<AudioSource>();
	}

    void OnMouseDown()
    {
        StartCoroutine("Pressed");
    }

    IEnumerator Pressed()
    {
        accepted.Play();
        this.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(loadingSceneIndex);
    }

}
