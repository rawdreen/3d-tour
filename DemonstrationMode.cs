﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class DemonstrationMode : MonoBehaviour {

    private AudioSource accepted;

	void Start () {
        accepted = GameObject.FindGameObjectWithTag("popup").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnMouseDown()
    {
        accepted.Play();
        GameObject.FindGameObjectWithTag("TimeLineMain").GetComponent<PlayableDirector>().Play();
    }
}
