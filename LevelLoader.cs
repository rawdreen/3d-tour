﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    public int loadIndex = 1;
    public Text loadText;
    public GameObject indicator;

	// Use this for initialization
	void Start () {
        indicator.GetComponent<Animator>().Play("LoadingStage");
        StartCoroutine(LoadAsc(loadIndex));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator LoadAsc(int loadIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(loadIndex);
        
        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadText.text = Mathf.Round(progress * 100).ToString() + " %";
            yield return null;
        }
    }
}
