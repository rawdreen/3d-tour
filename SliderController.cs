﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour {


    public Scrollbar scroll;
    public float speed = 0.02f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Control();
	}


    void Control()
    {
        if(Input.GetKey(KeyCode.S))
        {
            scroll.value += Time.deltaTime*speed;
        }
        if (Input.GetKey(KeyCode.W))
        {
            scroll.value -= Time.deltaTime*speed;
        }
    }
}
