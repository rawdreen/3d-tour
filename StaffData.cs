﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StaffData : MonoBehaviour {

    public GameObject name;
    public GameObject sureName;
    public GameObject department;
    public GameObject mail;
    public GameObject room;
    public GameObject officeHours;
    public GameObject post;

    public GameObject cardPreFab;
    public Transform parentObj;
	void Start () {
	}
	
	// Update is called once per frame
	public void SetStaffValues (string name, string sureName, string department, string mail, string room, string officeHours, string post) {
        this.name.GetComponent<Text>().text = name;
        this.sureName.GetComponent<Text>().text = sureName;
        this.department.GetComponent<Text>().text = department;
        this.mail.GetComponent<Text>().text = mail;
        this.room.GetComponent<Text>().text = room;
        this.officeHours.GetComponent<Text>().text = officeHours;
        this.post.GetComponent<Text>().text = post;
	}

    public void Clicked()
    {
        Debug.Log("Pressed");

        string name = this.name.GetComponent<Text>().text.ToString();
        string sureName = this.sureName.GetComponent<Text>().text.ToString();
        string department = this.department.GetComponent<Text>().text.ToString();
        string mail = this.mail.GetComponent<Text>().text.ToString();
        string room = this.room.GetComponent<Text>().text.ToString();
        string officeHours = this.officeHours.GetComponent<Text>().text.ToString();
        string post = this.post.GetComponent<Text>().text.ToString();

        GameObject tmpCard = Instantiate(cardPreFab);
        tmpCard.GetComponent<CardController>().Accept(name, sureName, mail, post, room, officeHours);

        tmpCard.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").GetComponent<Transform>());
        tmpCard.GetComponent<RectTransform>().localScale = new Vector3(8, 3, 1);
        tmpCard.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
    }
}
