﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map : MonoBehaviour {

    public int loadingSceneIndex = 1;
    AudioSource accepted;
	// Use this for initialization
	void Start () {
        accepted = GameObject.FindGameObjectWithTag("popup").GetComponent<AudioSource>();
	}

    void OnMouseDown()
    {
        StartCoroutine("Pressed");
    }

    IEnumerator Pressed()
    {
        accepted.Play();
        this.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(loadingSceneIndex);
    }
}
