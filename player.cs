﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{


    public GameObject g;
    public Rigidbody rig;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rig.transform.Translate(new Vector3(0, 0, 0.3f));
        }
        if (Input.GetKey(KeyCode.D))
        {
            rig.transform.Translate(new Vector3(0.3f, 0, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            rig.transform.Translate(new Vector3(-0.3f, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            rig.transform.Translate(new Vector3(0, 0, -0.3f));
        }
        if (Input.GetKey(KeyCode.E))
        {
            rig.transform.Rotate(new Vector3(0, 3f, 0));
        }
        if (Input.GetKey(KeyCode.Q))
        {
            rig.transform.Rotate(new Vector3(0, -3f, 0));
        }

    }
}