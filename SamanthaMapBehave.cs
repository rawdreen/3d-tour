﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using System.Linq;
using UnityEngine.Windows.Speech;
using UnityEngine.SceneManagement;


public class SamanthaMapBehave : MonoBehaviour {

    [Header("Sub Objects")]
    public GameObject[] element = new GameObject[3];
    public AudioSource click;

    public GameObject demo;

    public AudioSource[] timeLine;
    public Transform location;
    public Transform parent;
    GameObject tmpObj;

    public PlayableDirector[] directors;


    private bool sub_obj_pressed = false;

    private bool is_awake = true;


    KeywordRecognizer keyword;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    // Use this for initialization
    void Start()
    {
        keywords.Add("Give me information about University", Brief);
        keywords.Add("Give me information about B", BCorp);
        keywords.Add("Give me information about A", ACorp);
        keywords.Add("Back to main", MainMenu);

        keyword = new KeywordRecognizer(keywords.Keys.ToArray());
        keyword.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;


        StartCoroutine(Wait());

        location.GetComponent<Animator>().Play("accepted");
    }

    void Brief()
    {
        Demo();
    }

    void BCorp()
    {

    }

    void ACorp()
    {
        A();
    }

    void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void Update()
    {
        KeyBoard();
    }

    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    void OnMouseDown()
    {
        click.Play();

        float radius = 2f;
        if (sub_obj_pressed == false)
        {

            location.GetComponent<Animator>().Play("pressed");
            this.GetComponent<MeshRenderer>().enabled = true;
            Debug.Log("Pressed");
            for (int i = 0; i < element.Length; i++)
            {
                float angle = i * Mathf.PI * 2 / element.Length;
                Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius + parent.position.x, Mathf.Sin(angle) * radius + parent.position.y);
                tmpObj = Instantiate(element[i], newPos, Quaternion.identity);
                tmpObj.transform.SetParent(parent);
                tmpObj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            }
            sub_obj_pressed = true;
        }
        else
        {

            location.GetComponent<Animator>().Play("released");
            DestroySubObj();
        }
    }

    public void DestroySubObj()
    {
        this.GetComponent<MeshRenderer>().enabled = false;
        if (sub_obj_pressed == true)
        {
            for (int i = 0; i < GameObject.FindGameObjectsWithTag("box").Length; i++)
            {
                GameObject[] obj = GameObject.FindGameObjectsWithTag("box");
                Destroy(obj[i]);
            }
        }
        sub_obj_pressed = false;
    }


    void KeyBoard()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            Demo();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            directors[0].GetComponent<PlayableDirector>().Play();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            directors[0].GetComponent<PlayableDirector>().Play();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
           A();
        }
    }

    void Demo()
    {
        demo.SetActive(true);
        timeLine[0].Play();
        GameObject.FindGameObjectWithTag("TimeLineMain").GetComponent<PlayableDirector>().Play();
    }

    void A()
    {
        demo.SetActive(false);
        GameObject.FindGameObjectWithTag("TimeLineA").GetComponent<PlayableDirector>().Play();
        timeLine[1].Play();
    }

    IEnumerator Wait()
    {
        Debug.Log("Wait");
        yield return new WaitForSeconds(27f);
        keyword.Start();
        Debug.Log("Start");
    }

}
