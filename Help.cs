﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Help : MonoBehaviour {

    private string help_opened="HelpTab";
    public GameObject helpTab;
    public string helpType;
    AudioSource accepted;

	// Use this for initialization
    void Start () {
        accepted = GameObject.FindGameObjectWithTag("popup").GetComponent<AudioSource>();
    }

    void OnMouseDown()
    {
        HelpTab();
        accepted.Play();
        this.GetComponent<Animation>().Play();
        //if (GameObject.FindGameObjectWithTag("box") == true)
        //    for (int i = 0; i < GameObject.FindGameObjectsWithTag("box").Length; i++)
        //    {
        //        Destroy(GameObject.FindGameObjectWithTag("box"));
        //    }
    }

    public void HelpTab()
    {
        if(GameObject.FindGameObjectWithTag(help_opened)==false)
        {
            GameObject tmpObj = Instantiate(helpTab);
            help_opened = tmpObj.tag;
            Transform tmpParent = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Transform>();
            tmpObj.transform.SetParent(tmpParent);
            tmpObj.GetComponent<HelpPanelControl>().TakeHelp(helpType);
            tmpObj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            tmpObj.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            Destroy(GameObject.FindGameObjectWithTag(help_opened));
        }
    }
}
