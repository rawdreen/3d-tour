﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Windows.Speech;
using UnityEngine.SceneManagement;

public class SmanthaContactsBehave : MonoBehaviour
{

    [Header("Sub Objects")]
    public GameObject[] element = new GameObject[3];
    public AudioSource introduction;
    public AudioSource[] track;
    public Transform location;
    public Transform parent;
    public GameObject tmpObj;


    private bool sub_obj_pressed = false;

    private bool is_awake = true;


    KeywordRecognizer keyword;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    // Use this for initialization
    void Start()
    {
        keywords.Add("open a search panel", Find);  
        keywords.Add("close search", DestroySearch);
        keywords.Add("back to main", MainMenu);

        keyword = new KeywordRecognizer(keywords.Keys.ToArray());
        keyword.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        

        track[0].Play();
        location.GetComponent<Animator>().Play("accepted");

        FirstUsage();
        
    }

    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if(keywords.TryGetValue(args.text,out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FirstUsage()
    {
        if(SamathaBehave.contactListAccessed == false)
        {
            introduction.Play();
            StartCoroutine(Delay());
            SamathaBehave.contactListAccessed = true;
        }else
        {
            keyword.Start();
        }
    }

    void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(10f);
        keyword.Start();
        Debug.Log("Started");
    }

    void DestroySearch()
    {
        Destroy(GameObject.FindGameObjectWithTag("SearchPanel"));
    }
    void Find()
    {
        Debug.Log("ACCESSED");
        if (GameObject.FindGameObjectWithTag("SearchPanel") == false)
        {

            GameObject tmpGameObj = Instantiate(tmpObj, GameObject.FindGameObjectWithTag("Canvas").GetComponent<Transform>());
        }

    }

    void OnMouseDown()
    {
        track[1].Play();

        float radius = 1.7f;
        if (sub_obj_pressed == false)
        {

            location.GetComponent<Animator>().Play("pressed");
            this.GetComponent<MeshRenderer>().enabled = true;
            Debug.Log("Pressed");
            for (int i = 0; i < element.Length; i++)
            {
                float angle = i * Mathf.PI/ 2;
                Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius);
                tmpObj = Instantiate(element[i], newPos, Quaternion.identity);
                tmpObj.transform.SetParent(parent);
                tmpObj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            }
            sub_obj_pressed = true;
        }
        else
        {

            location.GetComponent<Animator>().Play("released");
            DestroySubObj();
        }
    }

    public void DestroySubObj()
    {
        this.GetComponent<MeshRenderer>().enabled = false;
        if (sub_obj_pressed == true)
        {
            for (int i = 0; i < GameObject.FindGameObjectsWithTag("box").Length; i++)
            {
                GameObject[] obj = GameObject.FindGameObjectsWithTag("box");
                Destroy(obj[i]);
            }
        }
        sub_obj_pressed = false;
    }
}
