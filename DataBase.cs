﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;

public class DataBase : MonoBehaviour
{


    private List<Staff> staff = new List<Staff>();
    private string connectionString;
    //public Transform parentObj;
    public GameObject staffPrefab;
    void Start()
    {
        connectionString = "URI= file:" + Application.dataPath + "/Database/Staff.sqlite";
    }


    public void GetData(string departmentType)
    {
        staff.Clear();
        DestroyData();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT * FROM staff WHERE department =\"" + departmentType + "\" ORDER BY name ASC";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        staff.Add(new Staff(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6)));
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }


        }

        SetData();
    }

    public void GetDataThroughName(string memberName)
    {
        staff.Clear();
        DestroyData();
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT * FROM staff WHERE name =\"" + memberName + "\" ORDER BY name ASC";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        staff.Add(new Staff(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6)));
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }


        }

        SetData();
    }

    public int GetSizeOfDB()
    {
        connectionString = "URI= file:" + Application.dataPath + "/Database/Staff.sqlite";

        int max_size=0;
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT COUNT(*) as total FROM staff";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        max_size = reader.GetInt32(0);
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }
            
        }
        return max_size;
    }
    public string GetNameData(int i)
    {
        connectionString = "URI= file:" + Application.dataPath + "/Database/Staff.sqlite";

        string [] tmpNameHolder = new string[0];

        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCommand = dbConnection.CreateCommand())
            {
                string query = "SELECT name FROM staff";
                dbCommand.CommandText = query;
                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    int d =0;
                    while (reader.Read())
                    {
                        tmpNameHolder[d] = reader.GetString(0);
                        d++;
                    }
                    dbConnection.Close();
                    reader.Close();
                }


            }
            return " ";
        }

    }

    void SetData()
    {
        for (int i = 0; i < staff.Count; i++)
        {
            GameObject tmpObj = Instantiate(staffPrefab);
            Staff tmpStaff = staff[i];

            tmpObj.GetComponent<StaffData>().SetStaffValues(tmpStaff.name, tmpStaff.sureName, tmpStaff.department, tmpStaff.mail, tmpStaff.room, tmpStaff.officeHours, tmpStaff.post);
            tmpObj.transform.SetParent(GameObject.FindGameObjectWithTag("ContactsPanel").GetComponent<Transform>());
            tmpObj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
    }

   public void DestroyData()
    {
        for (int i = 0; i < GameObject.FindGameObjectsWithTag("contacts").Length ; i++)
			{
                Debug.Log("Destroyed");
                GameObject[] obj = GameObject.FindGameObjectsWithTag("contacts");
                Destroy(obj[i]);
			}
    }
}
