﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Clock : MonoBehaviour
{

    public TextMesh time;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time.text = CurrentTime();
    }

    string CurrentTime()
    {
        string current_time;
        if (DateTime.Now.Hour < 10 && DateTime.Now.Minute < 10)
        {
             current_time = "0" + DateTime.Now.Hour.ToString() + " : 0" + DateTime.Now.Minute.ToString();
        }
         else if(DateTime.Now.Hour<10)
         {
              current_time = "0" + DateTime.Now.Hour.ToString() + " : " + DateTime.Now.Minute.ToString();
         }else if(DateTime.Now.Minute < 10)
         {
              current_time = DateTime.Now.Hour.ToString() + " : 0" + DateTime.Now.Minute.ToString();
         }
         else{
              current_time = DateTime.Now.Hour.ToString() + " : " + DateTime.Now.Minute.ToString();
         }
        return current_time;
        
    }
}
