﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepartmentController : MonoBehaviour {

    public GameObject departmentContainer;
    public GameObject departmentContentPanel;
    public Transform parent;

    public  int Tot_num_departments = 0; 

	void Start () {
        InstantiateDepartments();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void InstantiateDepartments()
    {
        Debug.Log("Done");
        for (int i = 0; i < Tot_num_departments; i++)
        {
            Debug.Log("Done");
            GameObject tmpDCP = Instantiate(departmentContentPanel, parent);
            tmpDCP.GetComponent<DepartmentContentPanel>().SetDepType(departmentContainer.GetComponent<DepartmentContainer>().GetDepartmentType(i));
        }
    }
}
