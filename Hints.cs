﻿using UnityEngine;
using UnityEngine.UI;

public class Hints : MonoBehaviour {

    public const int MAX_SIZE = 9;
    public float changeFreq = 21.5f;
    private float changeRate;

    public string[] hints = new string[MAX_SIZE];
    public Text hint_text;



	void Start () {
        changeRate = changeFreq;
        Random_Hints();
	}
	
	// Update is called once per frame
	void Update () {
        changeRate -= Time.deltaTime;
        Change_Hint();
	}

    void Random_Hints()
    {
        int rand = Random.Range(0, MAX_SIZE);
        hint_text.text = "say: \n" + hints[rand];
    }

    void Change_Hint()
    {
        
        if (changeRate < 0f)
        {
            Random_Hints();
            changeRate = changeFreq;
        }
    }
}
