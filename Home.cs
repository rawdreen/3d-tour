﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class Home : MonoBehaviour {

    public int loadingSceneIndex=0;
    AudioSource accepted;

	void Start () {
        accepted = GameObject.FindGameObjectWithTag("popup").GetComponent<AudioSource>();
	}

    void OnMouseDown()
    {
        StartCoroutine("Pressed");
    }

    IEnumerator Pressed()
    {

        accepted.Play();
        this.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(loadingSceneIndex);
        SamathaBehave.first_start = false;
    }
}
