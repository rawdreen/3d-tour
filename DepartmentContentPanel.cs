﻿using UnityEngine;
using UnityEngine.UI;


public class DepartmentContentPanel : MonoBehaviour {

    public Text departmentType;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDepType(string departmentType)
    {
        this.departmentType.text = departmentType;
    }

    public void OnMouseDown()
    {
        GameObject.FindGameObjectWithTag("DepartmentController").GetComponent<DataBase>().GetData(departmentType.text);
        Debug.Log("Worked");
    }
}
