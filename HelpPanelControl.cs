﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPanelControl : MonoBehaviour
{

    public HelpsLibrary helpsLibrary;

    public Transform parentObj;

    public GameObject commandTextObj;


    public static int MaxRange;
    string[] help = new string[MaxRange];
    string[] help_btn = new string[MaxRange];

    // Use this for initialization
    void Start()
    {

        InstatiateObj();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void GetSizeOfCmnds()
    {

    }

    public void TakeHelp(string help_type)
    {


        switch (help_type)
        {
            case "main":
                {
                    MaxRange = helpsLibrary.main_menu.Length;
                    help = new string[MaxRange];
                    help_btn = new string[MaxRange];

                    for (int i = 0; i < helpsLibrary.main_menu.Length; i++)
                    {
                        help[i] = helpsLibrary.Get_Main_Menu_Help(i);
                        help_btn[i] = helpsLibrary.Get_Main_Menu_btn(i);
                        //Debug.Log(help[i]);
                    }
                    break;
                }
            case "contacts":
                {
                    MaxRange = helpsLibrary.contatcs_menu.Length;
                    help = new string[MaxRange];
                    help_btn = new string[MaxRange];

                    for (int i = 0; i < helpsLibrary.contatcs_menu.Length; i++)
                    {
                        help[i] = helpsLibrary.Get_Contacts_Menu_Help(i);
                        help_btn[i] = helpsLibrary.Get_Contacts_Menu_btn(i);
                    }
                    break;
                }
            case "map":
                {
                    MaxRange = helpsLibrary.map_menu.Length;
                    help = new string[MaxRange];
                    help_btn = new string[MaxRange];

                    for (int i = 0; i < helpsLibrary.map_menu.Length; i++)
                    {
                        help[i] = helpsLibrary.Get_Map_Menu_Help(i);
                        help_btn[i] = helpsLibrary.Get_Map_Menu_btn(i);
                    }
                    break;
                }
        }
    }

    void InstatiateObj()
    {
        for (int i = 0; i < help.Length; i++)
        {
           GameObject tmpObj = Instantiate(commandTextObj,parentObj);
           tmpObj.GetComponent<HelpCommandPanel>().SetComponentsValues(help[i], help_btn[i]);
           //tmpObj.GetComponent<Transform>().SetParent(parentObj);

        }
    }
}
